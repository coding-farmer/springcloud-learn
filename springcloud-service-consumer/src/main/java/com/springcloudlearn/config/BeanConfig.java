package com.springcloudlearn.config;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RetryRule;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @Package: com.springcloudlearn.config
 * @ClassName: :BeanConfig
 * @Description: spirngcloud-learn
 * @Author: Coding-Farmer
 * @Date: 2019/8/1 14:46
 */
@Configuration
public class BeanConfig {
    /**
     * RestTemplate 该类是spring官方提供的，不是真正的spring cloud调
     */
    //使用Ribbon实现负载均衡调用,默认是轮询
    @LoadBalanced //加入ribbon的支持，那么在调用时，即可改为使用服务名称来访问
    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

    //覆盖掉原来Ribbon默认的轮询负载均衡策略
    @Bean
    public IRule iRule(){
        //采用随机的负载均衡策略
        //return new RandomRule();

        //采用重试的负载均衡策略，先按照RoundRobinRule（轮询）策略分发，
        // 如果分发到的服务不能访问，则在指定的时间内进行重试，
        // 如果还是不可用，则分发给其他可用的服务,默认阈值 是500ms
        RetryRule retryRule = new RetryRule();
        retryRule.setMaxRetryMillis(500);
        return retryRule;
    }

}
