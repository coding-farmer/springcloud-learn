package com.springcloudlearn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class SpringcloudAdminClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringcloudAdminClientApplication.class, args);
	}

}
