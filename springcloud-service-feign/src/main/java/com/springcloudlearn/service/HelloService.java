package com.springcloudlearn.service;

import com.springcloudlearn.fallback.MyFallback;
import com.springcloudlearn.fallbackfactory.MyFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Package: com.springcloudlearn.service
 * @ClassName: :HelloService
 * @Description: springcloud-learn
 * @Author: Coding-Farmer
 * @Date: 2019/8/7 17:50
 */
//使用feign的客户端注解绑定远程的名称，名称可以是大写，也可以小写fallback = MyFallback.class,
@FeignClient(value = "springcloud-service-provider", fallbackFactory = MyFallbackFactory.class)
public interface HelloService {

    //声明一个方法，这个方法就是远程的服务提供者提供的方法
    @RequestMapping("/provider/hello")
    public String hello();

}
