package com.springcloudlearn.fallbackfactory;

import com.springcloudlearn.service.HelloService;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * 获取远程的服务端异常信息
 */
/**
 * @Package: com.springcloudlearn.fallbackfactory
 * @ClassName: :MyFallbackFactory
 * @Description: springcloud-learn
 * @Author: Coding-Farmer
 * @Date: 2019/8/7 17:53
 */
@Component
public class MyFallbackFactory implements FallbackFactory<HelloService> {
    @Override
    public HelloService create(Throwable throwable) {

        return new HelloService() {
            @Override
            public String hello() {
                return throwable.getMessage();
            }
        };
    }
}
