package com.springcloudlearn.fallback;



import com.springcloudlearn.service.HelloService;
import org.springframework.stereotype.Component;


/**
 * 发生熔断，回调逻辑,获取不到服务端异常信息
 */
/**
 * @Package: com.springcloudlearn.fallback
 * @ClassName: :MyFallback
 * @Description: springcloud-learn
 * @Author: Coding-Farmer
 * @Date: 2019/8/7 17:50
 */
@Component
public class MyFallback implements HelloService {
    @Override
    public String hello() {
        return "远程服务不可用，暂时采用本地逻辑代替。。。。。";
    }
}
