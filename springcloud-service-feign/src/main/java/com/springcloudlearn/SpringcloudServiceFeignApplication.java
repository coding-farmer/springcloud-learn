package com.springcloudlearn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients //开启feign的客户端支持
@EnableCircuitBreaker
@SpringBootApplication
public class SpringcloudServiceFeignApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringcloudServiceFeignApplication.class, args);
	}

}
