package com.springcloudlearn.model;

/**
 * @Package: com.springcloudlearn.model
 * @ClassName: :User
 * @Description: spirngcloud-learn
 * @Author: Coding-Farmer
 * @Date: 2019/8/5 16:39
 */
public class User {
    private Integer  id;
    private String name;
    private String phone;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
