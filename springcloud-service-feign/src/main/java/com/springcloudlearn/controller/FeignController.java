package com.springcloudlearn.controller;

import com.springcloudlearn.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Package: com.springcloudlearn.controller
 * @ClassName: :FeignController
 * @Description: springcloud-learn
 * @Author: Coding-Farmer
 * @Date: 2019/8/7 17:41
 */
@RestController
@RequestMapping("/feign")
public class FeignController {

    @Autowired
    private HelloService helloService;

    @RequestMapping("/hello")
    public String hello() {
        //调用声明式接口方法,实现对远程服务的调用
        return helloService.hello();
    }

}
