package com.springcloudlearn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class SpringcloudEureka8701ServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringcloudEureka8701ServerApplication.class, args);
	}

}
