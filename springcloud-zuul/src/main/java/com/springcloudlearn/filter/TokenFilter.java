package com.springcloudlearn.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @Package: com.springcloudlearn.filter
 * @ClassName: :TokenFilter
 * @Description: springcloud-learn
 * @Author: Coding-Farmer
 * @Date: 2019/11/26 16:39
 */
@Component
public class TokenFilter  extends ZuulFilter {


    // 过滤器类型 pre 表示在 请求之前进行拦截
    @Override
    public String filterType() {
        return "pre";
    }

    // 过滤器的执行顺序。当请求在一个阶段的时候存在多个多个过滤器时，需要根据该方法的返回值依次执行
    @Override
    public int filterOrder() {
        return 0;
    }

    // 判断过滤器是否生效
    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        // 获取上下文
        RequestContext currentContext = RequestContext.getCurrentContext();
        HttpServletRequest request = currentContext.getRequest();
        String userToken = request.getParameter("userToken");
        if (StringUtils.isEmpty(userToken)) {
            //setSendZuulResponse(false)令zuul过滤该请求，不进行路由
            currentContext.setSendZuulResponse(false);
            //设置返回的错误码
            currentContext.setResponseStatusCode(401);
            currentContext.setResponseBody("userToken is null");
            return null;
        }
        // 否则正常执行业务逻辑.....
        return null;
    }
}
