package com.springcloudlearn.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @Package: com.springcloudlearn.config
 * @ClassName: :ZuulConfig
 * @Description: springcloud-learn
 * @Author: Coding-Farmer
 * @Date: 2019/12/2 15:42
 */
@Configuration
public class ZuulConfig {

    @RefreshScope
    @ConfigurationProperties("zuul")
    public ZuulProperties zuulProperties() {
        return new ZuulProperties();
    }
}
