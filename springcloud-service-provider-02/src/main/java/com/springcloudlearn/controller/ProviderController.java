package com.springcloudlearn.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Package: com.springcloudlearn.controller
 * @ClassName: :ProviderController
 * @Description: spirngcloud-learn
 * @Author: Coding-Farmer
 * @Date: 2019/8/1 14:58
 */
@RestController
@RequestMapping("/provider")
public class ProviderController {
    @RequestMapping("/hello")
    public String hello(){
        return "spring cloud provider-02 hello world";
    }
}
