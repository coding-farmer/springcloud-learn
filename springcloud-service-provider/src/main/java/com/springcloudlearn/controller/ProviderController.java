package com.springcloudlearn.controller;

import com.springcloudlearn.model.User;
import org.springframework.web.bind.annotation.*;

/**
 * @Package: com.springcloudlearn.controller
 * @ClassName: :SpringCloudProvider
 * @Description: spirngcloud-learn
 * @Author: Coding-Farmer
 * @Date: 2019/8/1 13:59
 */
@RestController
@RequestMapping("/provider")
public class ProviderController {

    @RequestMapping("/hello")
    public String hello() throws InterruptedException {
        //hystrix默认1000ms触发熔断，消费者已经改为3500触发熔断
        //Thread.sleep(4000);
        //Thread.sleep(2000);
        //int a=10/0;//除数不能为0,这一行代码就会触发熔断
        return "spring cloud provider-01 hello world";
    }

    @GetMapping("/user")
    public User user(){
        //这里省略业务逻辑
        System.out.println("服务提供者1 user。。。。。。。");
        User user=new User();
        user.setId(1);
        user.setName("zhangsan");
        user.setPhone("1");
        return user;
    }

    @GetMapping("/getUser")
    public  User getUser(@RequestParam("id") Integer id, @RequestParam("name") String name, @RequestParam("phone") String phone){
        //这里省略业务逻辑
        System.out.println("服务提供者1 user。。。。。。。");
        User user=new User();
        user.setId(id);
        user.setName(name);
        user.setPhone(phone);
        return user;
    }


    @PostMapping("/addUser")
    public  User addUser(@RequestParam("id") Integer id,@RequestParam("name") String name,@RequestParam("phone") String phone){
        //这里省略业务逻辑
        System.out.println("服务提供者1 user。。。。。。。");
        User user=new User();
        user.setId(id);
        user.setName(name);
        user.setPhone(phone);
        //将user对象插入数据库
        return user;
    }


    @PutMapping("/updateUser")
    public  User updateUser(@RequestParam("id") Integer id,@RequestParam("name") String name,@RequestParam("phone") String phone){
        //这里省略业务逻辑
        System.out.println("服务提供者1 user。。。。。。。ID:"+id+"name:"+name+"phone:"+phone);
        User user=new User();
        user.setId(id);
        user.setName(name);
        user.setPhone(phone);
        //将user对象插入数据库
        return user;
    }


    @DeleteMapping("/deleteUser")
    public  User deleteUser(@RequestParam("id") Integer id,@RequestParam("name") String name,@RequestParam("phone") String phone){
        //这里省略业务逻辑
        System.out.println("服务提供者1 user。。。。。。。ID:"+id+"name:"+name+"phone:"+phone);
        User user=new User();
        user.setId(id);
        user.setName(name);
        user.setPhone(phone);
        //将user对象插入数据库
        return user;
    }


}
