package com.springcloudlearn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class SpringcloudConsumerSleuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringcloudConsumerSleuthApplication.class, args);
	}

}
