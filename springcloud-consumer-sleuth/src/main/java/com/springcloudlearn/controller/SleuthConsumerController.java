package com.springcloudlearn.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.logging.Logger;

/**
 * @Package: com.springcloudlearn.controller
 * @ClassName: :SleuthConsumerController
 * @Description: springcloud-learn
 * @Author: Coding-Farmer
 * @Date: 2019/8/9 17:00
 */
@RestController
@RequestMapping("/consumer")
public class SleuthConsumerController {
    private static final Logger logger = Logger.getLogger(SleuthConsumerController.class.getName());

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("/hello")
    public String hello() {
        logger.info("=== consumer hello ===");
         return   restTemplate.getForEntity("http://springcloud-provider-sleuth/provider/hello",String.class).getBody();
    }



}
