package com.springcloudlearn.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @Package: com.springcloudlearn.config
 * @ClassName: :BeanConfig
 * @Description: springcloud-learn
 * @Author: Coding-Farmer
 * @Date: 2019/8/9 17:05
 */
@Configuration
public class BeanConfig {

    @LoadBalanced //加入ribbon的支持，那么在调用时，即可改为使用服务名称来访问
    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

}
