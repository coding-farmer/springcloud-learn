package com.springcloudlearn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.turbine.EnableTurbine;

@EnableDiscoveryClient //和该注解@EnableEurekaClient功能一样 //开启eureka客户端功能
@EnableTurbine //开启turibine相关支持
@SpringBootApplication
public class SpringcloudHystrixTurbineApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringcloudHystrixTurbineApplication.class, args);
	}

}
