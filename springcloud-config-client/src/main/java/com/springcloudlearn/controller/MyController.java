package com.springcloudlearn.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Package: com.springcloudlearn.controller
 * @ClassName: :MyController
 * @Description: springcloud-learn
 * @Author: Coding-Farmer
 * @Date: 2019/8/15 15:42
 */
@RestController
@RefreshScope //实现动态刷新的注解,不然客户端不知道刷新哪里
public class MyController {

    @Value("${env}")
    private String env;

    @RequestMapping("/index")
    public String env(){
        return env;
    }
}
