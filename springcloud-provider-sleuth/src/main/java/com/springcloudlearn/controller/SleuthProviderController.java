package com.springcloudlearn.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.logging.Logger;

/**
 * @Package: com.springcloudlearn.controller
 * @ClassName: :SleuthProviderController
 * @Description: springcloud-learn
 * @Author: Coding-Farmer
 * @Date: 2019/8/9 16:59
 */
@RestController
@RequestMapping("/provider")
public class SleuthProviderController {
    private final Logger logger = Logger.getLogger(SleuthProviderController.class.getName());

    @RequestMapping("/hello")
    public String hello(HttpServletRequest request){
        logger.info("=== provider hello ===,Traced={"+request.getHeader("X-B3-TraceId")+"},SpanId={"+request.getHeader("X-B3-SpanId")+"}");
        return "Trace";
    }

}
